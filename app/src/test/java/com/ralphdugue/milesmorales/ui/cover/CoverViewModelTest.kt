package com.ralphdugue.milesmorales.ui.cover

import com.ralphdugue.milesmorales.data.MarvelAPI
import com.ralphdugue.milesmorales.data.responses.ComicImage
import com.ralphdugue.milesmorales.data.responses.ComicResponse
import com.ralphdugue.milesmorales.data.responses.DataContainer
import com.ralphdugue.milesmorales.data.responses.MarvelResponseWrapper
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.Assert
import org.junit.Test
import retrofit2.Response

@OptIn(ExperimentalCoroutinesApi::class)
internal class CoverViewModelTest {

    lateinit var subject: CoverViewModel

    @RelaxedMockK
    private val marvelAPI = mockk<MarvelAPI>()

    private val comicResponse = ComicResponse(
        title = "title",
        description = "description",
        thumbnail = ComicImage(
            path = "path",
            extension = "jpg"
        )
    )
    private val wrapper = MarvelResponseWrapper(
        code = 200,
        status = "Ok",
        data = DataContainer(count = 1, results = listOf(comicResponse))
    )

    private val model = CoverModel.fromResponse(comicResponse)

    private val comicId = "12345"

    private fun configureMocks() {
        val testDispatcher = UnconfinedTestDispatcher()
        Dispatchers.setMain(testDispatcher)
        subject = CoverViewModel(marvelAPI, testDispatcher)
    }

    @Test
    fun `When a LoadComic event executes successfully, the state updates to ShowingCover`() = runTest {
        coEvery { marvelAPI.getComicById(comicId) } returns Response.success(wrapper)
        configureMocks()
        subject.onEvent(CoverEvent.LoadComic(comicId))

        Assert.assertEquals(subject.state.value, CoverState.ShowingCover(model))
        Dispatchers.resetMain()
    }
}