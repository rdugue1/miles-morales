package com.ralphdugue.milesmorales.data.responses

data class ComicResponse(
    val title: String?,
    val description: String?,
    val thumbnail: ComicImage
)

data class  ComicImage(
    val path: String?,
    val extension: String?
)
