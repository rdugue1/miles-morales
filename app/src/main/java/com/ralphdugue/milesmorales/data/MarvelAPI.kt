package com.ralphdugue.milesmorales.data

import com.ralphdugue.milesmorales.data.responses.ComicResponse
import com.ralphdugue.milesmorales.data.responses.MarvelResponseWrapper
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface MarvelAPI {

    @GET("comics/{id}")
    suspend fun getComicById(
        @Path("id") comicId: String
    ): Response<MarvelResponseWrapper<ComicResponse>>
}