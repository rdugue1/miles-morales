package com.ralphdugue.milesmorales.data.responses

data class MarvelResponseWrapper<T>(
    val code: Int,
    val status: String,
    val data: DataContainer<T>
)

data class DataContainer<T>(
    val count: Int,
    val results: List<T>
)