package com.ralphdugue.milesmorales

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.remember
import com.ralphdugue.milesmorales.ui.cover.CoverEvent
import com.ralphdugue.milesmorales.ui.cover.CoverScreen
import com.ralphdugue.milesmorales.ui.cover.CoverViewModel
import com.ralphdugue.milesmorales.ui.theme.MilesMoralesTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel by viewModels<CoverViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MilesMoralesTheme {
                CoverScreen(viewModel = viewModel)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.onEvent(CoverEvent.LoadComic("57006"))
    }
}

