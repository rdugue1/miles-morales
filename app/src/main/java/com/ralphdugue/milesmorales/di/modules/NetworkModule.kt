package com.ralphdugue.milesmorales.di.modules

import android.os.Build
import androidx.annotation.RequiresApi
import com.ralphdugue.milesmorales.BuildConfig
import com.ralphdugue.milesmorales.data.MarvelAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.math.BigInteger
import java.security.MessageDigest
import java.time.Instant
import java.time.format.DateTimeFormatter

@Module
@InstallIn(SingletonComponent::class)
internal object NetworkModule {

    private const val API_KEY = BuildConfig.API_KEY
    private const val PRIVATE_KEY = BuildConfig.PRIVATE_KEY
    private const val BASE_URL = "https://gateway.marvel.com:443/v1/public/"

    @RequiresApi(Build.VERSION_CODES.O)
    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor { chain ->
            val ts = DateTimeFormatter.ISO_INSTANT.format(Instant.now()).toString()
            val hash = md5("$ts$PRIVATE_KEY$API_KEY")
            val newUrl = chain.request().url.newBuilder()
                .addQueryParameter("ts", ts)
                .addQueryParameter("apikey", API_KEY)
                .addQueryParameter("hash", hash)
                .build()
            val newRequest = chain.request().newBuilder()
                .url(newUrl)
                .build()
            chain.proceed(newRequest)
        }
        .build()

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .client(okHttpClient)
        .build()

    @Provides
    fun provideMarvelAPI(retrofit: Retrofit): MarvelAPI = retrofit.create(MarvelAPI::class.java)

    private fun md5(input: String): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }
}