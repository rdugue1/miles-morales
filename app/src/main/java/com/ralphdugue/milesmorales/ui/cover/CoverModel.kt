package com.ralphdugue.milesmorales.ui.cover

import com.ralphdugue.milesmorales.data.responses.ComicResponse

data class CoverModel(
    val title: String?,
    val description: String?,
    val imageUrl: String?
) {
    companion object {
        fun fromResponse(comicResponse: ComicResponse) = with(comicResponse) {
            CoverModel(
                title = title,
                description = description,
                imageUrl = "https${thumbnail.path?.substring(4)}.${thumbnail.extension}"
            )
        }
    }
}
