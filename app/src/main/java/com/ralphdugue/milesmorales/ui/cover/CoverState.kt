package com.ralphdugue.milesmorales.ui.cover

sealed class CoverState {

    object Loading : CoverState()

    data class ShowingCover(val data: CoverModel) : CoverState()

    object Error : CoverState()
}

sealed class CoverEvent {

    data class LoadComic(val comicId: String) : CoverEvent()
}