package com.ralphdugue.milesmorales.ui.cover

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest

@Composable
fun CoverScreen(viewModel: CoverViewModel) {
    viewModel.state.value.let { value ->
        when (value) {
            is CoverState.ShowingCover -> CoverContent(coverModel = value.data)
            CoverState.Error -> EmptyMessage(message = "There was an error!")
            CoverState.Loading -> EmptyMessage(message = "Loading...")
        }
    }
}

@Composable
private fun CoverContent(coverModel: CoverModel) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.Black),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        coverModel.title?.let {
            Box(
                modifier = Modifier
                    .background(color = Color.Black)
                    .padding(10.dp)
                    .wrapContentSize(),
            ) {
                Text(
                    text = it,
                    style = MaterialTheme.typography.h6,
                    color = Color.White,
                    maxLines = 1,
                    textAlign = TextAlign.Center,
                )
            }
        }
        Divider(color = MaterialTheme.colors.primary, thickness = 2.dp)
        Card {
            AsyncImage(
                modifier = Modifier.height(500.dp).fillMaxWidth(),
                model = ImageRequest.Builder(LocalContext.current)
                    .data(coverModel.imageUrl)
                    .crossfade(true)
                    .build(),
                contentDescription = "Cover image"
            )
        }
        Divider(color = MaterialTheme.colors.primary, thickness = 2.dp)
        coverModel.description?.let {
            Box(
                modifier = Modifier
                    .background(color = Color.Black)
                    .padding(horizontal = 15.dp)
                    .wrapContentWidth()
                    .fillMaxHeight(),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = it,
                    style = MaterialTheme.typography.h6,
                    color = Color.White,
                    textAlign = TextAlign.Center,
                )
            }
        }
    }
}

@Composable
fun EmptyMessage(message: String) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(text = message)
    }
}