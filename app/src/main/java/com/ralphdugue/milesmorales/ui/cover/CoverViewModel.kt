package com.ralphdugue.milesmorales.ui.cover

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ralphdugue.milesmorales.data.MarvelAPI
import com.ralphdugue.milesmorales.data.responses.ApiError
import com.ralphdugue.milesmorales.data.responses.ApiException
import com.ralphdugue.milesmorales.data.responses.ApiSuccess
import com.ralphdugue.milesmorales.data.responses.handleApi
import com.ralphdugue.milesmorales.di.modules.IoDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CoverViewModel @Inject constructor(
    private val marvelAPI: MarvelAPI,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _state = mutableStateOf<CoverState>(CoverState.Loading)
    val state: State<CoverState>
        get() = _state

    fun onEvent(event: CoverEvent) {
        when (event) {
            is CoverEvent.LoadComic -> loadComic(event.comicId)
        }
    }

    private fun loadComic(comicId: String) {
        viewModelScope.launch(ioDispatcher) {
            when (val result = handleApi { marvelAPI.getComicById(comicId) }) {
                is ApiError -> _state.value = CoverState.Error
                is ApiException -> _state.value = CoverState.Error
                is ApiSuccess -> {
                    val data = CoverModel.fromResponse(
                        comicResponse = result.data.data.results.first()
                    )
                    _state.value = CoverState.ShowingCover(data)
                }
            }
        }
    }
}