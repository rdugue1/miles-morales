package com.ralphdugue.milesmorales

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MilesApplication : Application() {
}