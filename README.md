# MilesMorales

This is an example project as part of the interview process for Disney. It is implemented using Jetpack Compose and MVI. It retrieves the title, cover image, and description of a comic from the marvel API, and displays it to the user.

## Dependencies

- Dagger Hilt for dependency injection
- Network:
  - Retrofit
  - Okhttp
  - Moshi
- UI
  - Jetpack Compose
  - Coil
- Testing
  - MockK
  - Coroutines Testing APIs

## Possible Improvements

- CLEAN Architecture / Domain Layer
- Repository Pattern / Caching
- More testing
- Better / more dynamic UI

## API Credentials

Add your `API_KEY` and `PRIVATE_KEY` to your local properties file so they can be used to authorize API requests.